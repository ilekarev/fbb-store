var gulp          = require('gulp'),
    postcss       = require('gulp-postcss'),
    browserSync   = require('browser-sync'),
    autoprefixer  = require('autoprefixer'),
    cssnext       = require('cssnext'),
    precss        = require('precss'),
    minmax        = require('postcss-media-minmax'),
    atImport      = require('postcss-import');
    jshint        = require('gulp-jshint');

gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: 'app'
    },
    notify: false
  });
});

gulp.task('lint', function() {
  return gulp.src('app/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('postcss', function(){
  var processors = [
        atImport(),
        cssnext({
          'customProperties': true,
          'colorFunction': true,
          'customSelectors': true,
        }),
        autoprefixer({browsers: ['> 1%']}),
        minmax(),
        precss()
      ];

  return gulp.src('app/postcss/*.css')
          .pipe(postcss(processors))
          .pipe(gulp.dest('app/css'))
          .pipe(browserSync.reload({stream: true}))
});

gulp.task('watch', ['postcss', 'browser-sync'], function(){
  gulp.watch('app/postcss/*.css', ['postcss']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/*.js', browserSync.reload);
});

gulp.task('default', ['watch']);