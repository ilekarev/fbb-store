var LEKAREV = {};

// LEKAREV.book       - Книга
// LEKAREV.delivery   - Способы доставки
// LEKAREV.payment    - Способы оплаты

LEKAREV.order = {};
LEKAREV.order.delivery = {};
LEKAREV.order.payment = {};
LEKAREV.order.manager = 'soaring775@yandex.ru';

function getBook() {
  var xhrBook = new XMLHttpRequest();

  xhrBook.onreadystatechange = function () {
      if (xhrBook.readyState !== 4) {
          return false;
      }
      if (xhrBook.status !== 200) {
          alert("Error, status code: " + xhrBook.status);
          return false;
      }

      LEKAREV.book = JSON.parse(xhrBook.responseText);
      LEKAREV.order.book = LEKAREV.book.id;
      LEKAREV.order.payment.currency = LEKAREV.book.currency;

      var oldnode = document.getElementById('order'),
            clone = oldnode.cloneNode(true);

      var bookImage = clone.querySelector('.book_image'),
          bookTitle = clone.querySelector('.book_title'),
          price     = LEKAREV.book.price,
          order = clone.querySelector('.total');

      bookImage.src = LEKAREV.book.cover.large;
      bookTitle.innerHTML = LEKAREV.book.title;
      order.innerHTML = 'Итого к оплате ' + price + ' <s>Z</s>';

      oldnode.parentNode.replaceChild(clone, oldnode);

      conslog();

      //Отправка формы

      LEKAREV.btn = clone.querySelector('.button');
      LEKAREV.btn.addEventListener('click', function(e) {
        e.preventDefault();
        var order = LEKAREV.order;
        var addressDisplay = document.getElementById('adress');
        var status = document.getElementById("status");
        var form = document.getElementById('order');

        form.style.display = 'none';
        status.style.display = 'block';
        status.innerHTML = 'Попытка отправки данных';
      
        order.name     = document.getElementById('name').value;
        order.phone    = document.getElementById('telephone').value;
        order.email    = document.getElementById('email').value;
        order.comment  = document.getElementById('review').value;
        order.delivery.address  = addressDisplay.value;

        function sendOrder(order) {
          var xhr = new XMLHttpRequest();
          xhr.onreadystatechange = function(){
            if (xhr.readyState == 4){ 
              LEKAREV.call = JSON.parse(xhr.responseText);
              if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304){
                status.innerHTML = "Заказ успешно оформлен";
                status.style.color = "green";
              } else {
                  status.innerHTML = "Ошибка оформления заказа, попробуйте ещё";
                  status.style.color = "red";
                  form.style.display = 'block';
              }
            }
          };

          xhr.open('POST', 'https://netology-fbb-store-api.herokuapp.com/order', true);
          xhr.setRequestHeader('Content-Type', 'application/JSON');
          xhr.send(JSON.stringify(order));
        }

        if (order.name && order.phone && order.email && order.delivery.id && order.payment.id) {
          if (addressDisplay.style.display == 'block' && order.delivery.address) {
            sendOrder(order);
          } else if (addressDisplay.style.display != 'block') {
            order.delivery.address = "";
            sendOrder(order);
          } else {
            alert("Оформление не завершено. Заполните форму заказа");
          }
        } else {
          alert("Оформление не завершено. Заполните форму заказа");
        }

      });

  };

  var id = "fbb-0001";
  if (window.location.search) {
    id = window.location.search.slice(1);
  }

  xhrBook.open("GET", "https://netology-fbb-store-api.herokuapp.com/book/" + id, true); 
  xhrBook.send("");
}

// name: payment, либо delivery
function getRadio(name, xhr) {
  if (name != 'delivery' && name != 'payment') {
    return false;
  }

  xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function () {
    if (xhr.readyState !== 4) {
        return false;
    }
    if (xhr.status !== 200) {
        alert("Error, status code: " + xhr.status);
        return false;
    }

    var str = JSON.parse(xhr.responseText),
        frag = document.createDocumentFragment(),
        list = document.getElementById(name),
        input,
        label;

    if (name == 'delivery') {
        LEKAREV.delivery = str;
      } else if (name == 'payment') {
        LEKAREV.payment = str;
      }

    str.forEach(function(e, i) {
      input = document.createElement('input');
      input.id = 'radio_' + e.id;
      input.type = 'radio';
      input.name = 'radio_' + name;

      label = document.createElement('label');
      label.htmlFor = 'radio_' + e.id;

      if (name == 'delivery') {
        label.innerHTML = e.name;
      } else if (name == 'payment') {
        label.innerHTML = e.title;
      }

      frag.appendChild(input);
      frag.appendChild(label);
      
    });

    if (name == 'delivery') {
        input = document.createElement('input');
        input.id = 'adress';
        input.setAttribute('data-visible','hide');
        input.type = 'text';
        input.name = 'adress';
        input.placeholder = 'Адрес';
        label = document.createElement('label');
        label.setAttribute('data-visible','hide');
        label.htmlFor = 'adress';
        label.innerHTML = 'Адрес доставки';
        frag.appendChild(label);
        frag.appendChild(input);
      } 

    list.appendChild(frag);

  };

  xhr.open("GET", "https://netology-fbb-store-api.herokuapp.com/order/" + name, true); 
  xhr.send("");
}

// Функция поиска по атрибуту
function getElementsByAttribute(attribute) {
  var matchingElements = [];
  var allElements = document.getElementsByTagName('*');
  for (var i = 0, n = allElements.length; i < n; i++) {
    if (allElements[i].getAttribute(attribute) !== null) {
      matchingElements.push(allElements[i]);
    }
  }
  return matchingElements;
}

function conslog() {
  var delivery = document.getElementById('delivery'),
      radelivery = document.getElementsByName('radio_delivery'),
      payment = document.getElementById('payment'),
      rapayment = document.getElementsByName('radio_payment');
      order = document.querySelector('.total');

  delivery.addEventListener('click', function(e){
    radelivery.forEach(function(e, i) {
      if (e.type == 'radio' && e.checked) {
        var checked = e.id.slice(6);
        var price = LEKAREV.delivery[i].price + LEKAREV.book.price;
        LEKAREV.payment.forEach(function(e, i) {
          rapayment[i].disabled = !e.availableFor.includes(checked);
          rapayment[i].checked = false;
        });
        order.innerHTML = 'Итого к оплате ' + price + ' <s>Z</s>';
        var hide = getElementsByAttribute('data-visible');
        if (LEKAREV.delivery[i].needAdress) {
          hide.forEach(function(e) {
            e.style.display = 'block';
          });
        } else {
          hide.forEach(function(e) {
            e.style.display = 'none';
          });
        } 
        LEKAREV.order.delivery.id = checked;
      }
    });
  });
  payment.addEventListener('click', function(e){
    rapayment.forEach(function(e, i) {
      if (e.type == 'radio' && e.checked) {
        LEKAREV.order.payment.id = e.id.slice(6);
      }
    });
  });
}

window.onload = function(e) {
  e.preventDefault();
  var xhrDelivery, xhrPayment;
  
  getRadio('delivery', xhrDelivery);
  getRadio('payment', xhrPayment);
  setTimeout(getBook(), 1000); 

};

function clickSearch(value) {
    var target = value.target;
    var classV = target.classList.value;
    if (classV == "enter" || classV == "search_input" && value.keyCode == 13) {
        var string = document.querySelector(".search_input").value;
        value.preventDefault();
        location.href = "index.html?" + encodeURIComponent(string);
    }
}
LEKAREV.search = document.querySelector(".search");
LEKAREV.search.addEventListener('click', clickSearch);
LEKAREV.search.addEventListener('keydown', clickSearch);