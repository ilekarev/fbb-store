var LEKAREV = {};

LEKAREV.listBook    = document.querySelector('#list_book');
LEKAREV.search      = document.querySelector(".search");
LEKAREV.iBegin      = 0;

function removeChildrenRecursively(node) {
  if (!node) return;
  while (node.hasChildNodes()) {
    removeChildrenRecursively(node.firstChild);
    node.removeChild(node.firstChild);
  }
}

function findBook(string) {
    removeChildrenRecursively(LEKAREV.listBook);
    var frag = document.createDocumentFragment();
    var a = document.createElement("a");
    a.innerHTML = "Ещё несколько книг";
    a.classList.add("btn");
    a.style.display = 'none';
    frag.appendChild(a);
    LEKAREV.book.forEach(function(e) {
        var t = e.title,
            a = e.author.name,
            d = e.description,
            f0 = e.features[0].title,
            f1 = e.features[1].title,
            i = e.id,
            r0 = e.reviews[0].cite,
            r1 = e.reviews[1].cite;
        if (t.includes(string) || a.includes(string) || d.includes(string) ||
            f0.includes(string) || f1.includes(string) || i.includes(string) ||
            r0.includes(string) || r1.includes(string)) {
            frag.appendChild(getBook(e));
        }
    });
    LEKAREV.listBook.appendChild(frag);
}

function getBook(e) {
    var sec, // <section>
        img, // <img>
        div, // <div>

    sec = document.createElement('section');
    sec.classList.add('book');
    sec.setAttribute('data-id', e.id);
    img = document.createElement('img');
    img.classList.add('book_mini-cover');
    img.src = e.cover.small;
    div = document.createElement('div');
    div.classList.add('book_info');
    div.innerHTML = e.info;
    sec.appendChild(img);
    sec.appendChild(div);
    return sec;
}

function showListBooks(target, iBegin,iEnd) {
    var frag = document.createDocumentFragment(),
        mFrag = LEKAREV.book.slice(iBegin,iEnd);

    if (LEKAREV.book.length <= iEnd) {
        target.style.display = 'none';
    }

    mFrag.forEach(function (e, i) {
        frag.appendChild(getBook(e));
    });
    return frag;
}

function getListBook() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState !== 4) {
            return false;
        }
        if (xhr.status !== 200) {
            alert("Error, status code: " + xhr.status);
            return false;
        }
        LEKAREV.book = JSON.parse(xhr.responseText);
        LEKAREV.listBook.appendChild(showListBooks("", 0, 4));
        if (window.location.search) {
            var string = decodeURIComponent(window.location.search).slice(1);
            findBook(string);
        }
    };
    xhr.open("GET", "https://netology-fbb-store-api.herokuapp.com/book/", true); 
    xhr.send("");
}

window.onload = function() {
    getListBook();
};

LEKAREV.listBook.addEventListener('click', function(value) {
    var target    = value.target,
        classBook = document.querySelectorAll('.book');

    function setHref(e, i) {
            if (LEKAREV.listBook.children[i+1] == target.parentNode) {
                location.href = "book.html?" + target.parentNode.getAttribute('data-id');
            }
        }

    while (!target.classList.contains('book')) {
        if (target.classList.contains('btn')) {
            LEKAREV.iBegin += 4;
            var iBegin = LEKAREV.iBegin;
            LEKAREV.listBook.appendChild(showListBooks(target, iBegin, iBegin + 4));
            return;
        }
        classBook.forEach(setHref);
        target = target.parentNode;
        if (!target.classList) {
            return;
        }
    }
});

function clickSearch(value) {
    var target = value.target;
    var classV = target.classList.value;
    if (classV == "enter" || classV == "search_input" && value.keyCode == 13) {
        var string = document.querySelector(".search_input").value;
        value.preventDefault();
        findBook(string);
    }
}

LEKAREV.search.addEventListener('click', clickSearch);
LEKAREV.search.addEventListener('keydown', clickSearch);