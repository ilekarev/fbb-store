var LEKAREV = {};

LEKAREV.btn = document.querySelector('.btn');

window.onload = function() {  

  var xhr =  new XMLHttpRequest();        

  xhr.onreadystatechange = function () {
      if (xhr.readyState !== 4) {
          return false;
      }
      if (xhr.status !== 200) {
          alert("Error, status code: " + xhr.status);
          return false;
      }

      LEKAREV.book = JSON.parse(xhr.responseText);

      var oldnode = document.getElementById('book'),
            clone = oldnode.cloneNode(true);

      var bookImage           = clone.querySelector('.book_image'),
          bookDescrition      = clone.querySelector('.book_descrition'),
          bookReviewItems     = clone.querySelectorAll('.book_review_item'),
          bookAdvantageItems  = clone.querySelectorAll('.book_advantage_item'),
          btn                 = clone.querySelector('.btn'),
          price               = 0;

      bookImage.src = LEKAREV.book.cover.large;
      bookDescrition.innerHTML = LEKAREV.book.description;
      bookReviewItems.forEach(function(e, i) {
        e.querySelector('img').src = LEKAREV.book.reviews[i].author.pic;
        e.querySelector('p').innerHTML = LEKAREV.book.reviews[i].cite;
      });
      bookAdvantageItems.forEach(function(e, i) {
        e.querySelector('img').src = LEKAREV.book.features[i].pic;
        e.querySelector('p').innerHTML = LEKAREV.book.features[i].title;
      });
      btn.innerHTML = "<span>Купить</span> за жалкие " + price + " <s>Z</s>";

      oldnode.parentNode.replaceChild(clone, oldnode);

      getPrice();

      blinkEye();

      var pupil = document.querySelector('.pupil'),
          bookItem = document.querySelector('.book_item'),
          bookEye = document.querySelector('.eye');

      // Движение зрачка
      document.addEventListener('mousemove', function(e) {
      var x, y, proc2, proc3;
        x = bookItem.offsetLeft + bookEye.offsetLeft + 69; // 100% от левого края окна до центра зрачка
        y = bookItem.offsetTop + bookEye.offsetTop + 69; // 100% от верхнего края окна до центра зрачка
        
        //Высчитываем на сколько процентов от центра зрачка к краю окна находится курсор
        proc2 = 100 - e.pageX / (x / 100);
        proc3 = 100 - e.pageY / (y / 100);

        // Для вычислений гипотенузы
        sqrt1 = (e.pageX*(x / 100))*(e.pageX*(x / 100));
        sqrt2 = (e.pageY*(y / 100))*(e.pageY*(y / 100));
        var c = Math.sqrt(proc2*proc2 + proc3*proc3);
        // Если гипотенуза больше 95%, то ничего не делаем,
        // если меньше, двигаем зрачок (в процентном соотношении)
        if (c <= 95) {
          pupil.style.left = (100 - proc2) * 0.53 + 'px';
          pupil.style.top = (100 - proc3) * 0.53 + 'px';
        } 
      });

  };
  var id = "fbb-0001"; // По умолчанию загрузить первую книгу
  if (window.location.search) {
    id = window.location.search.slice(1);
  }

  xhr.open("GET", "https://netology-fbb-store-api.herokuapp.com/book/" + id, true); 
  xhr.send("");
};

function getPrice() {

  var xhr =  new XMLHttpRequest();
  var priceValue;

  xhr.onreadystatechange = function () {
      if (xhr.readyState !== 4) {
          return false;
      }
      if (xhr.status !== 200) {
          alert("Price error, status code: " + xhr.status);
          return false;
      }

      LEKAREV.currency = JSON.parse(xhr.responseText);

      var price = LEKAREV.book.price;

      LEKAREV.btn = document.querySelector('.btn');
      LEKAREV.btn.innerHTML = "<span>Купить</span> за жалкие " + price + " <s>Z</s>";

      LEKAREV.btn.addEventListener("click", function(){
        location.href = "order.html" + window.location.search;
      }, false);
  };

  xhr.open("GET", "https://netology-fbb-store-api.herokuapp.com/currency/", true); 
  xhr.send("");

}

function blinkEye() {
  var e = document.querySelector(".eyelid");
  e.addEventListener("animationiteration", function(e) {
    e.target.classList.add('anim');
    setTimeout(function() {
      e.target.classList.remove('anim');
    }, (Math.random() + 5) * 1000);
  }, false);
}

function clickSearch(value) {
    var target = value.target;
    var classV = target.classList.value;
    if (classV == "enter" || classV == "search_input" && value.keyCode == 13) {
        var string = document.querySelector(".search_input").value;
        value.preventDefault();
        location.href = "index.html?" + encodeURIComponent(string);
    }
}
LEKAREV.search = document.querySelector(".search");
LEKAREV.search.addEventListener('click', clickSearch);
LEKAREV.search.addEventListener('keydown', clickSearch);